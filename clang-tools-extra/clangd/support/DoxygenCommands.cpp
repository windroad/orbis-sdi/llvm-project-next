//===--- DoxygenCommands.cpp -------------------------------------*- C++-*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#include "support/DoxygenCommands.h"
#include "clang/Basic/CharInfo.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/Support/Compiler.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/FormatVariadic.h"

#define SUPPORTED_SHORTCODES      "abcep"
#define WHITESPACES               " \t\r\n"
#define COMMAND_INDICATORS        "\\@"

namespace clang {
namespace clangd {
namespace doxygen {

bool isSupportedShortCommand(char Code) {
  constexpr llvm::StringLiteral SupportedCodes(SUPPORTED_SHORTCODES);
  return SupportedCodes.find(Code) != llvm::StringRef::npos;
}

// Test whether <Contents contains special short codes (Doxygen style).
bool hasShortCommands(llvm::StringRef Contents) {
  auto FindShortCode = [&](llvm::StringRef Prefixes) {
    size_t I = Contents.find_first_of(Prefixes);
    if (I == llvm::StringRef::npos)
      return false;
    if (I + 1 == Contents.size())
      return false;
    return true;
  };

  return FindShortCode(COMMAND_INDICATORS);
}

// Translate all supported doxygen short commands into markdown constructs.
void translateCommandsToMarkdown(std::string *R, llvm::StringRef Text) {
  if (LLVM_UNLIKELY(Text.empty()))
    return;

  for (size_t I = 0, J = 0; I < Text.size(); ) {
    // Locate the nearest command indicator
    J = Text.find_first_of(COMMAND_INDICATORS, I);
    if (J == llvm::StringRef::npos || Text.size() - J < 2) {
      R->append(Text.data() + I, Text.size() - I);
      break;
    } else {
      if (J > I)
        R->append(Text.data() + I, J - I);
      I = J++;
    }

    char Code = Text[J++];
    if (!isWhitespace(Text[J])) {
      // At least one whitespace is needed between the commmand and the word,
      // otherwise leave it as what it is.
      J = Text.find_first_not_of(WHITESPACES, J);
      if (J == llvm::StringRef::npos)
        J = Text.size();
      R->append(Text.data() + I, J - I);
      I = J;
      continue;
    } else if (!isSupportedShortCommand(Code)) {
      // For unsupported comamnds, leave it as what it is.
      R->append(Text.data() + I, J - I);
      I = J;
      continue;
    }
    // Locate the Word and save it
    llvm::StringRef Word;
    I = J;
    J = Text.find_first_not_of(WHITESPACES, J + 1);
    if (J != llvm::StringRef::npos) {
      I = J;
      J = Text.find_first_of(WHITESPACES, I + 1);
      if (J == llvm::StringRef::npos)
        J = Text.size();
      Word = Text.substr(I, J - I);
      I = J;
    } else {
      // Preserve the whitespaces
      if (J > I)
        R->append(Text.data() + I, Text.size() - I);
      break;
    }
    // Apply the command
    if (!Word.empty()) {
      switch (Code) {
      case 'a': case 'e':
        // Italic style
        R->append(llvm::formatv("*{0}*", Word));
        break;
      case 'b':
        // Bold style
        R->append(llvm::formatv("**{0}**", Word));
        break;
      case 'c': case 'p':
        // inline code
        R->append(llvm::formatv("`{0}`", Word));
        break;
      default:
        llvm_unreachable("unhandled doxygen short command");
      }
    }
  }
}

std::string translateCommandsToMarkdown(llvm::StringRef Text) {
  std::string R;
  translateCommandsToMarkdown(&R, Text);
  return R;
}

// Discard all doxygen-like short codes from the Text.
std::string translateCommandsToPlainText(llvm::StringRef Text) {
  std::string R;

  if (Text.size() < 4)
    return Text.str();

  for (size_t I = 0, J = 0; I < Text.size(); ) {
    J = Text.find_first_of(COMMAND_INDICATORS, I);
    if (J == llvm::StringRef::npos) {
      R.append(Text.data() + I, Text.size() - I);
      break;
    } else if (Text.size() - J >= 2) {
      if (J > I)
        R.append(Text.data() + I, J - I);
      I = J + 2;
    } else {
      I++;
    }
  }
  return R;
}

} // namespace doxygen
} // namespace clangd
} // namespace clang

//===--- DoxygenCommands.h ---------------------------------------*- C++-*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANGD_DOXYGENCOMMANDS_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANGD_DOXYGENCOMMANDS_H

#include "llvm/ADT/StringRef.h"

namespace clang {
namespace clangd {
namespace doxygen {

bool isSupportedShortCommand(char Code);
bool hasShortCommands(llvm::StringRef Text);
std::string translateCommandsToMarkdown(llvm::StringRef Text);
void translateCommandsToMarkdown(std::string *Buffer, llvm::StringRef Text);
std::string translateCommandsToPlainText(llvm::StringRef Text);

} // namespace doxygen
} // namespace clangd
} // namespace clang

#endif

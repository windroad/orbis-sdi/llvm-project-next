//===--- DoxygenCommentBlock.cpp ----------------------------*- C++-*------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#include "DoxygenCommands.h"
#include "DoxygenCommentBlock.h"
#include "Logger.h"
#include "Markup.h"
#include "clang/Basic/CharInfo.h"
#include "llvm/ADT/None.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/Support/Compiler.h"
#include <string>
#include <vector>

#define WHITESPACES     " \r\n\v\t"

namespace clang {
namespace clangd {
namespace {

// Originally from Hover.cpp
// If the backtick at `Offset` starts a probable quoted range, return the range
// (including the quotes).
llvm::Optional<llvm::StringRef> getBacktickQuoteRange(llvm::StringRef Line,
                                                      unsigned Offset) {
  assert(Line[Offset] == '`');

  // The open-quote is usually preceded by whitespace.
  llvm::StringRef Prefix = Line.substr(0, Offset);
  constexpr llvm::StringLiteral BeforeStartChars = " \t(=";
  if (!Prefix.empty() && !BeforeStartChars.contains(Prefix.back()))
    return llvm::None;

  // The quoted string must be nonempty and usually has no leading/trailing ws.
  auto Next = Line.find('`', Offset + 1);
  if (Next == llvm::StringRef::npos)
    return llvm::None;
  llvm::StringRef Contents = Line.slice(Offset + 1, Next);
  if (Contents.empty() || isWhitespace(Contents.front()) ||
      isWhitespace(Contents.back()))
    return llvm::None;

  // The close-quote is usually followed by whitespace or punctuation.
  llvm::StringRef Suffix = Line.substr(Next + 1);
  constexpr llvm::StringLiteral AfterEndChars = " \t)=.,;:";
  if (!Suffix.empty() && !AfterEndChars.contains(Suffix.front()))
    return llvm::None;

  return Line.slice(Offset, Next + 1);
}

void renderText(llvm::StringRef Text, markup::Paragraph &Out) {
scan_next:
  if (Text.empty())
    return;

  for (unsigned I = 0; I < Text.size(); ++I) {
    switch (Text[I]) {
    case '`':
      if (auto Range = getBacktickQuoteRange(Text, I)) {
        Out.appendText(Text.substr(0, I));
        Out.appendCode(Range->trim("`"), /*Preserve=*/true);
        Text = Text.substr(I + Range->size());
        goto scan_next;
      }
      break;
    }
  }
  Out.appendText(Text).appendSpace();
}

} // anonymous namespace

/// Parse parameter information given by @param and @tparam.
void DoxygenCommentBlock::parseParam(llvm::StringRef Input, bool IsTemplate) {
  std::string ParamName;
  auto InsertParameter = [](std::vector<Param> &L, std::string &Name,
                            llvm::StringRef Desc) {
    Param NewParam;
    NewParam.Name = Name;
    NewParam.Description = Desc.str();
    L.emplace_back(NewParam);
  };
  ::size_t I = Input.find_first_of(WHITESPACES);

  if (I != llvm::StringRef::npos) {
    ParamName = Input.substr(0, I).str();
    Input = Input.substr(I + 1).ltrim();

    if (IsTemplate) {
      if (TemplateParameters == llvm::None)
        TemplateParameters = std::vector<Param>();
      InsertParameter(*TemplateParameters, ParamName, Input);
    } else {
      if (Parameters == llvm::None)
        Parameters = std::vector<Param>();
      InsertParameter(*Parameters, ParamName, Input);
    }
  }
}

void DoxygenCommentBlock::parseLine(llvm::StringRef Line) {
  llvm::StringRef CommandName, Payload;

  if (Line[0] == '@' || Line[0] == '\\') {
    size_t I = Line.find_first_of(WHITESPACES);
    if (I == llvm::StringRef::npos)
      I = Line.size();
    CommandName = Line.substr(1, I - 1);
    if (I < Line.size())
      Payload = Line.substr(I + 1).ltrim();
  }

  if (CommandName.startswith("param")) {
    LastCommand = CommandParam;
    parseParam(Payload);

  } else if (CommandName.startswith("tparam")) {
    LastCommand = CommandTParam;
    parseParam(Payload, true);

  } else if (CommandName.equals("return")) {
    LastCommand = CommandReturn;
    if (Returns == llvm::None)
      Returns = std::vector<std::string>();
    Returns->push_back(Payload.str());

  } else if (CommandName.equals("note")) {
    LastCommand = CommandNote;
    if (Notes == llvm::None)
      Notes = std::string();
    Notes->append(Payload.str());

  } else if (CommandName.equals("code")) {
    // This command is handled by render function
    if (LastCommand == CommandBrief)
      Brief.push_back("@code");

  } else if (CommandName.equals("endcode")) {
    // This command is handled by render function
    if (LastCommand == CommandBrief)
      Brief.push_back("@endcode");

  } else {
    switch (LastCommand) {
    case CommandBrief:
      Brief.push_back(Line.str());
      break;
    case CommandParam:
    case CommandTParam:
      {
        auto UpdateLastParameter = [&](std::vector<Param> &L) {
          auto &P = L.back();
          assert(P.size() > 0);
          if (P.Description->size())
            P.Description->append(" ");
          P.Description->append(Line.str());
        };
        UpdateLastParameter(LastCommand == CommandParam ? *Parameters : *TemplateParameters);
      }
      break;
    case CommandReturn:
      {
        auto &S = Returns->back();
        if (!S.empty())
          S.append(" ");
        S.append(Line.str());
      }
      break;
    case CommandNote:
      if (!Notes->empty())
        Notes->append(" ");
      Notes->append(Line.str());
      break;
    }
  }
}

void DoxygenCommentBlock::parse(llvm::StringRef Input) {
  llvm::StringRef Line, Rest;

  for (std::tie(Line, Rest) = Input.split('\n');
       !(Line.empty() && Rest.empty());
       std::tie(Line, Rest) = Rest.split('\n')) {

    Line = Line.ltrim();
    if (!Line.empty()) {
      parseLine(Line);
    } else
      Brief.push_back("");
  }
}

void DoxygenCommentBlock::render(markup::Document &Output, bool CompressBrief) {
  bool HasNotes = Notes && !Notes->empty();

  LastCommand = CommandBrief;
  if (!Brief.empty() || HasNotes) {
    markup::Section* S = nullptr;
    // This is used when CompressBrief is true.
    markup::Paragraph *CurrentP = nullptr;
    unsigned EmptyLines = 0;

    if (!Brief.empty()) {
      S = &Output.addSection("");
      bool InCodeBlockRange = false;

      for (const std::string &L : Brief) {
        if (!L.empty()) {
          if (EmptyLines)
            S->addParagraph(); // Insert an empty paragraph as newline
          EmptyLines = 0;
          // @code and @endcode are handled here.
          if (L == "@code") {
            if (InCodeBlockRange == false) {
              InCodeBlockRange = true;
              S->addParagraph().appendMarker("```", true/*Preserve*/);
            } else
              S->addParagraph().appendText(L);
            CurrentP = nullptr;
          } else if (L == "@endcode") {
            if (LLVM_LIKELY(InCodeBlockRange == true)) {
              InCodeBlockRange = false;
              S->addParagraph().appendMarker("```", true/*Preserve*/);
            } else
              S->addParagraph().appendText(L);
            CurrentP = nullptr;
          } else if (InCodeBlockRange) {
            S->addParagraph().appendRawText(L);
            CurrentP = nullptr;
          } else {
            if (CompressBrief) {
              if (CurrentP == nullptr)
                CurrentP = &S->addParagraph();
              renderText(L, *CurrentP);
            } else
              renderText(L, S->addParagraph());
          }
        } else {
          EmptyLines++;
          CurrentP = nullptr;
        }
      }
      if (InCodeBlockRange) {
        S->addParagraph().appendMarker("```", true);
      }
    }
    if (HasNotes) {
      if (S == nullptr)
        S = &Output.addSection("");
      S->addParagraph().appendItalicText(*Notes);
    }
  }

  if (TemplateParameters && !TemplateParameters->empty()) {
    markup::Section &S = Output.addSection("Template Parameters:");
    for (const auto &Param : *TemplateParameters) {
      markup::Paragraph &P = S.addParagraph();
      P.appendCode(*Param.Name);
      P.appendText(" - ");
      if (!Param.Description->empty())
        renderText(*Param.Description, P);
      else
        P.appendText("No description given.");
    }
  }

  if (Parameters && !Parameters->empty()) {
    markup::Section &S = Output.addSection("Parameters:");
    for (const auto &Param : *Parameters) {
      markup::Paragraph &P = S.addParagraph();
      P.appendCode(*Param.Name);
      P.appendText(" - ");
      if (Param.Description && !Param.Description->empty())
        renderText(*Param.Description, P);
      else
        P.appendText("No description given.");
    }
  }

  if (Returns && !Returns->empty()) {
    markup::Section &S = Output.addSection("Returns:");
    for (const auto &D : *Returns)
      renderText(D, S.addParagraph());
  }
}

std::string DoxygenCommentBlock::renderBrief() {
  std::string R;
  unsigned EmptyLines = 0;
  for (const std::string &L : Brief) {
    if (!L.empty()) {
      if (EmptyLines) {
        R.append("  \n  \n");
        EmptyLines = 0;
      }
      R.append(doxygen::translateCommandsToMarkdown(L));
      R.append("  \n");
    } else
      EmptyLines++;
  }
  return R;
}

} // namespace clangd
} // namespace clang

//===--- DoxygenCommentBlock.h -----------------------------------*- C++-*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANGD_DOXYGENCOMMENTBLOCK_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANGD_DOXYGENCOMMENTBLOCK_H

#include "Markup.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringRef.h"
#include <string>
#include <vector>

namespace clang {
namespace clangd {

struct DoxygenCommentBlock {
  /// Represents parameters of a function, a template or a macro.
  /// For example:
  /// - void foo(ParamType Name = DefaultValue)
  /// - #define FOO(Name)
  /// - template <ParamType Name = DefaultType> class Foo {};
  struct Param {
    /// None for unnamed parameters.
    llvm::Optional<std::string> Name;
    /// None if no parameter description provided.
    llvm::Optional<std::string> Description;
  };

  std::vector<std::string> Brief;
  llvm::Optional<std::vector<std::string>> Returns;
  /// Set for functions, lambdas and macros with parameters.
  llvm::Optional<std::vector<Param>> Parameters;
  /// Set for all templates(function, class, variable).
  llvm::Optional<std::vector<Param>> TemplateParameters;
  llvm::Optional<std::string> Notes;

  void parse(llvm::StringRef Input);
  void render(markup::Document &Output, bool CompressBrief = false);
  std::string renderBrief();

private:
  enum {
    CommandBrief,
    CommandParam,
    CommandTParam,
    CommandReturn,
    CommandNote
  } LastCommand = CommandBrief;
  void parseLine(llvm::StringRef Line);
  void parseParam(llvm::StringRef Line, bool IsTemplate = false);
};

} // namespace clangd
} // namespace clang

#endif
//===-- ProjectInfoYAMLTests.cpp ------------------------------------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#include "windroad/ProjectInfo.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

namespace clang {
namespace clangd {
namespace windroad {

namespace {
using ::testing::ElementsAre;

MATCHER_P(Val, Value, "") {
  if (*arg == Value)
    return true;
  *result_listener << "value is " << *arg;
  return false;
}

MATCHER_P2(PairVal, Value1, Value2, "") {
  if (*arg.first == Value1 && *arg.second == Value2)
    return true;
  *result_listener << "values are [" << *arg.first << ", " << *arg.second
                   << "]";
  return false;
}

TEST(ProjectInfoYAML, SyntacticForms) {
  ProjectInfo P;
  const char *YAML = R"yaml(
Project:
  Identifier: 'org.example.test'
  VersionData: VEhJUyBJUyBURVNUIFZFUlNJT04gREFUQQ
---
Compilation:
  C:
    Arguments: [fooC, barC]
    HeaderSearchDirs: [/path0, path1]
  CPP:
    Arguments: [fooCPP, barCPP]
    HeaderSearchDirs: [/path/CPP, path1/CPP]
  Default:
    Arguments: [fooDefault, barDefault]
    HeaderSearchDirs: [/path/default, lib/default]
  )yaml";

  ProjectInfo::parseYAML(YAML, ".ProjectInfo", P);
  EXPECT_FALSE(P.NoCompilationInfo);
  EXPECT_THAT(P.Project, ElementsAre(
    PairVal("Identifier", "org.example.test"),
    PairVal("VersionData", "VEhJUyBJUyBURVNUIFZFUlNJT04gREFUQQ")
  ));
  EXPECT_THAT(P.Compilation.C.Arguments, ElementsAre(
    Val("fooC"), Val("barC")
  ));
  EXPECT_THAT(P.Compilation.C.HeaderSearchDirs, ElementsAre(
    Val("/path0"), Val("path1")
  ));
  EXPECT_THAT(P.Compilation.CPP.Arguments, ElementsAre(
    Val("fooCPP"), Val("barCPP")
  ));
  EXPECT_THAT(P.Compilation.CPP.HeaderSearchDirs, ElementsAre(
    Val("/path/CPP"), Val("path1/CPP")
  ));
  EXPECT_THAT(P.Compilation.Default.Arguments, ElementsAre(
    Val("fooDefault"), Val("barDefault")
  ));
  EXPECT_THAT(P.Compilation.Default.HeaderSearchDirs, ElementsAre(
    Val("/path/default"), Val("lib/default")
  ));
}

TEST(ProjectInfoYAML, NoCompilation) {
  ProjectInfo P;
  const char *YAML = R"yaml(
Project:
  Identifier: org.example.test
Compilation:
  Default:
    HeaderSearchDirs: []
    Arguments: []
  )yaml";

  ProjectInfo::parseYAML(YAML, ".ProjectInfo", P);
  EXPECT_TRUE(P.NoCompilationInfo);
}

} // namespace
} // namespace windroad
} // namespace clangd
} // namespace clang

//===-- DoxygenCommandsTests.cpp ------------------------------------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
#include "support/DoxygenCommands.h"
#include "llvm/ADT/StringRef.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <cstring>

namespace clang {
namespace clangd {
namespace doxygen {

// Forward prototype
bool isSupportedShortCommand(char Code);


TEST(DoxygenCommands, ToMarkdown) {
#define RT(in, exp) T = in; EXPECT_EQ(translateCommandsToMarkdown(T), exp)
  // Empty string remains empty
  std::string T = "";
  EXPECT_TRUE(translateCommandsToMarkdown(T).empty());

  // Whitespaces should not be touched
  EXPECT_EQ(translateCommandsToMarkdown(" \t\n\n \n"), " \t\n\n \n");
  EXPECT_EQ(translateCommandsToMarkdown("    \t  \n "), "    \t  \n ");

  // Supported short commands
  bool M[128];
  ::memset(M, false, sizeof(M));
  M[static_cast<int>('a')] = true;
  M[static_cast<int>('b')] = true;
  M[static_cast<int>('c')] = true;
  M[static_cast<int>('e')] = true;
  M[static_cast<int>('p')] = true;
  for (int I = 1; I < 128; ++I) {
    EXPECT_EQ(isSupportedShortCommand(static_cast<char>(I)), M[I]);
  }

  // Incomplete comamnds of supported ones should be discarded.
  RT(R"( \a )", R"(  )");

  // Incomplete commands of unknown ones should be preserved.
  RT(R"( \f Hello)", R"( \f Hello)");
  RT(R"(\q @0 \1)", R"(\q @0 \1)");

  // Commands in bad formats are preserved.
  RT(R"(\a123 \a\b456)", R"(\a123 \a\b456)");
  RT(R"(@param File The file path )", R"(@param File The file path )");

  // '\a' and \e' should be translated into italic constructs.
  RT(R"(\a 123 \e 456)", R"(*123* *456*)");
  RT(R"(\a \a 123)", R"(*\a* 123)");
  RT(R"(@e \aABC)", R"(*\aABC*)");

  // '\b' is rendered in bold font style.
  RT(R"(\b 123)", R"(**123**)");

  // '\c' and '\p' are rendered as inline codes.
  RT(R"(\c abcd @p efgh)", R"(`abcd` `efgh`)");

#undef RT
}

} // namespace doxygen
} // namespace clangd
} // namespace clang

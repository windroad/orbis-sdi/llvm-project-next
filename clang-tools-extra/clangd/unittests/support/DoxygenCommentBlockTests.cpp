//===--- DoxygenCommentBlockTests.cpp ----------------------------*- C++-*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
#include "support/DoxygenCommentBlock.h"
#include "support/Markup.h"
#include "llvm/ADT/StringRef.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <libxml/parser.h>
#include <vector>

namespace clang {
namespace clangd {
namespace {

void resetDocumentBlock(DoxygenCommentBlock &D) {
  D.Brief.clear();
  if (D.Returns)
    D.Returns->clear();
  if (D.Parameters)
    D.Parameters->clear();
  if (D.TemplateParameters)
    D.TemplateParameters->clear();
  if (D.Notes)
    D.Notes->clear();
}

std::string renderCommentsAsMarkdown(llvm::StringRef Comments, bool CompressBrief = false) {
  DoxygenCommentBlock D;
  std::string R;
  markup::Document Doc;

  D.parse(Comments);
  D.render(Doc, CompressBrief);
  return Doc.asMarkdown();
}

TEST(DoxygenCommentBlockTests, ParseBrief) {
  DoxygenCommentBlock D;
  std::string Comments;
  std::vector<std::string> Expected;

  Comments = R"(We have never been to Asia, nor have we visited Africa.
Seek success, but always be prepared for random cats.
She always had an interesting perspective on why the world must be flat.
I'll have you know I've written over fifty novels
Shakespeare was a famous 17th-century diesel mechanic.)";
  Expected = {
    "We have never been to Asia, nor have we visited Africa.",
    "Seek success, but always be prepared for random cats.",
    "She always had an interesting perspective on why the world must be flat.",
    "I'll have you know I've written over fifty novels",
    "Shakespeare was a famous 17th-century diesel mechanic."
  };
  D.parse(Comments);
  EXPECT_EQ(D.Brief, Expected);

  resetDocumentBlock(D);
  Comments = R"(\brief Line 1
Line2

@brief Line 3
Line4

  )";
  Expected = {
    "Line1",
    "Line2",
    "",
    "Line3",
    "Line4"
    ""
  };
  D.parse(Comments);
  EXPECT_EQ(D.Brief, Expected);

  resetDocumentBlock(D);
  Comments = "\t\t  \n"
             "      "
             "\t  \t ";
  D.parse(Comments);
  Expected = {"", "", ""};
  EXPECT_EQ(D.Brief, Expected);

  resetDocumentBlock(D);
  Comments = "@brief He embraced his new life as an eggplant.\n"
             "\n"
             "@param A the parameter a.\n"
             "@note abcdefg";
  Expected = {
    "He embraced his new life as an eggplant.",
    ""
  };
  D.parse(Comments);
  EXPECT_EQ(D.Brief, Expected);
}

TEST(DoxygenCommentBlockTests, RenderBrief) {
  std::string Comments, Expected;

  Comments = "Joyce enjoyed eating @b pancakes with @b ketchup.\n"
             "As he looked out the window, he saw a clown walk by.\n"
             "\n"
             "\n"
             "\n"
             "Mary plays the @p piano.";
  Expected = "Joyce enjoyed eating **pancakes** with **ketchup.**\n"
             "As he looked out the window, he saw a clown walk by.\n"
             "\n"
             "Mary plays the piano.";
  EXPECT_EQ(renderCommentsAsMarkdown(Comments), Expected);

  Comments = "@brief She was the type of girl who wanted to live in a pink house.\n"
             "Not all people who wander are lost.\n"
             "\n"
             "@code\n"
             "TEST ABCD\n"
             "@endcode";
  Expected = "he was the type of girl who wanted to live in a pink house.\n"
             "Not all people who wander are lost.\n"
             "  \n"
             "```\n"
             "TEST ABCD\n"
             "```";
  EXPECT_EQ(renderCommentsAsMarkdown(Comments), Expected);
}

} // namespace
} // namespace clangd
} // namespace clang

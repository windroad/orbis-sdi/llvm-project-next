//===--- ProjectInfo.h ----------------------------------------- -*- C++-*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_TOOLS_EXTRA_CLANGD_WINDROAD_PROJECTINFO_H
#define LLVM_CLANG_TOOLS_EXTRA_CLANGD_WINDROAD_PROJECTINFO_H

#include "llvm/ADT/StringRef.h"
#include <vector>

namespace clang {
namespace clangd {
namespace windroad {

struct ProjectInfo {
  /// Parses project info from a YAML file (one from each --- delimited document).
  /// Documents that contained fatal errors are omitted from the results.
  /// BufferName is used for the SourceMgr and diagnostics.
  static void parseYAML(llvm::StringRef YAML,
                        llvm::StringRef BufferName, ProjectInfo &Out);

  std::string Directory;
  bool NoCompilationInfo = true;

  struct ProjectBlock {
    llvm::Optional<std::string> Identifier;
    llvm::Optional<std::string> VersionData;
  };
  ProjectBlock Project;

  struct CompilationBlock {
    struct LanguageBlock {
      llvm::Optional<std::vector<std::string>> Arguments;
      llvm::Optional<std::vector<std::string>> HeaderSearchDirs;
    };
    LanguageBlock C;
    LanguageBlock CPP;
    LanguageBlock Default;
  };
  CompilationBlock Compilation;
};

} // namespace windroad
} // namespace clangd
} // namespace clang

#endif

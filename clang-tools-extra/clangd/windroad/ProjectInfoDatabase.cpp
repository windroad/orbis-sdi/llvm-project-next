//===- ProjectInfoDatabase.cpp ----------------------------------*- C++ -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
#include "ProjectInfoDatabase.h"
#include "support/Logger.h"
#include "llvm/ADT/SmallString.h"
#include "llvm/Support/Path.h"
#include <iterator>

namespace clang {
namespace clangd {
namespace windroad {
namespace {

llvm::Optional<tooling::CompileCommand>
buildCompileCommand(ProjectInfo &P, PathRef File) {
  std::vector<std::string> Argv;

  if (P.NoCompilationInfo)
    return llvm::None;
  
  auto FileExtension = llvm::sys::path::extension(File);
  bool IsCFile = FileExtension.equals_insensitive(".c");

  Argv.push_back(IsCFile ? "clang" : "clang++");
  // Clang treats .h files as C by default and files without extension as linker
  // input, resulting in unhelpful diagnostics.
  // Parsing as Objective C++ is friendly to more cases.
  if (FileExtension.empty() || FileExtension == ".h")
    Argv.push_back("-xobjective-c++-header");
  
  if (IsCFile) {
    if (P.Compilation.C.HeaderSearchDirs)
      llvm::copy(*P.Compilation.C.HeaderSearchDirs, std::back_inserter(Argv));
    else if (P.Compilation.Default.HeaderSearchDirs)
      llvm::copy(*P.Compilation.Default.HeaderSearchDirs, std::back_inserter(Argv));
    if (P.Compilation.C.Arguments && !P.Compilation.C.Arguments->empty())
      llvm::copy(*P.Compilation.C.Arguments, std::back_inserter(Argv));
  } else {
    if (P.Compilation.CPP.HeaderSearchDirs)
      llvm::copy(*P.Compilation.CPP.HeaderSearchDirs, std::back_inserter(Argv));
    else if (P.Compilation.Default.HeaderSearchDirs)
      llvm::copy(*P.Compilation.Default.HeaderSearchDirs, std::back_inserter(Argv));
    if (P.Compilation.CPP.Arguments && !P.Compilation.CPP.Arguments->empty())
      llvm::copy(*P.Compilation.CPP.Arguments, std::back_inserter(Argv));
  }

  Argv.push_back(std::string(File));
  tooling::CompileCommand Cmd(llvm::sys::path::parent_path(File),
                              llvm::sys::path::filename(File), std::move(Argv),
                              /*Output=*/"");
  Cmd.Heuristic = "Windroad ProjectInfo Database";

  return Cmd;
}

} // namespace

llvm::Optional<tooling::CompileCommand>
ProjectInfoDatabase::getCompileCommand(PathRef File) const {
  namespace path = llvm::sys::path;

  // Compute absolute paths to all ancestors (substrings of P.Path).
  llvm::SmallVector<llvm::StringRef, 8> Ancestors;
  for (auto Ancestor = absoluteParent(File); !Ancestor.empty();
      Ancestor = absoluteParent(Ancestor)) {
    Ancestors.emplace_back(Ancestor);
  }
  // Ensure corresponding cache entries exist in the map.
  llvm::SmallVector<ProjectInfoFileCache*, 8> Caches;
  {
    std::lock_guard<std::mutex> Lock(Mu);
    for (llvm::StringRef Ancestor : Ancestors) {
      auto It = Cache.find(Ancestor);
      if (It == Cache.end()) {
        llvm::SmallString<512> ProjectInfoPath = Ancestor;
        path::append(ProjectInfoPath, ".ProjectInfo");
        path::native(ProjectInfoPath);
        It = Cache.try_emplace(Ancestor, ProjectInfoPath.str(), Ancestor).first;
      }
      Caches.push_back(&It->second);
    }
  }
  // Query each .ProjectInfo file and merge the configs
  ProjectInfo P;
  for (auto *Cache : Caches)
    Cache->get(TFS,
               std::chrono::steady_clock::now() - std::chrono::seconds(30),
               P);
  
  return buildCompileCommand(P, File);
}

} // namespace windroad
} // namespace clangd
} // namespace clang

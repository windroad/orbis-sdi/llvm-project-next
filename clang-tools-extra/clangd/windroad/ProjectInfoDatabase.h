//===- ProjectInfoDatabase.h ------------------------------------*- C++ -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_WINDROAD_PROJECTINFODATABASE_H
#define LLVM_WINDROAD_PROJECTINFODATABASE_H

#include "ProjectInfo.h"
#include "support/FileCache.h"
#include "support/Path.h"
#include "clang/Tooling/CompilationDatabase.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/Support/FormatVariadic.h"
#include "llvm/Support/Path.h"
#include <iterator>
#include <mutex>
#include <vector>

namespace clang {
namespace clangd {
class ThreadsafeFS;
namespace windroad {

/// Provides compilation arguments from .ProjectInfo file (if any),
/// when no compilation database is available.
class ProjectInfoDatabase {
public:
  ProjectInfoDatabase(const ThreadsafeFS &TFS) : TFS(TFS) {}

  llvm::Optional<tooling::CompileCommand> getCompileCommand(PathRef File) const;

private:
  /// Threadsafe cache around reading a .ProjectInfo file from disk.
  class ProjectInfoFileCache : public FileCache {
    mutable ProjectInfo PI;
    std::string Directory;

  public:
    ProjectInfoFileCache(llvm::StringRef Path, llvm::StringRef Directory) 
        : FileCache(Path), Directory(Directory) {}

    void get(const ThreadsafeFS &TFS, 
             std::chrono::steady_clock::time_point FreshTime,
             ProjectInfo &Out) {
      read(TFS, FreshTime,
        [&](llvm::Optional<llvm::StringRef> Data) {
          if (Data) {
            ProjectInfo::parseYAML(*Data, path(), PI);
            PI.Directory = Directory;
          } else
            PI.NoCompilationInfo = true;
        },
        [&]() {
          auto UpdateString = [](llvm::Optional<std::string> &F,
                               llvm::Optional<std::string> &T,
                               bool NoMerge = false) {
            if (F && !F->empty()) {
              if (T == llvm::None)
                T = std::string(*F);
              else if (T->empty())
                T->assign(*F);
              else if (NoMerge == false)
                T->append(*F);
              return false;
            }
            return true;
          };
          using OptionalVector = llvm::Optional<std::vector<std::string>>;
          auto UpdateVector = [](OptionalVector &F, OptionalVector &T) {
            if (F && !F->empty()) {
              if (T == llvm::None)
                T = *F;
              else
                llvm::copy(*F, std::back_inserter(*T));
              return false;
            }
            return true;
          };
          auto UpdateHeaderSearchDirs = [&](OptionalVector &F, OptionalVector &T) {
            if (F && !F->empty()) {
              if (T == llvm::None)
                T = std::vector<std::string>(F->size());
              for (std::string &D : *F) {
                if (llvm::sys::path::is_relative(D))
                  T->push_back(std::move(llvm::formatv("-I{0}/{1}", Directory, D)));
                else
                  T->push_back(std::move(llvm::formatv("-I{0}", D)));
              }
              return false;
            }
            return true;
          };
          using LanguageBlock = ProjectInfo::CompilationBlock::LanguageBlock;
          auto UpdateLanguageBlock = [&UpdateVector, &UpdateHeaderSearchDirs]
                                     (LanguageBlock &F, LanguageBlock &T) {
            bool R = true; // No updates were made?
            R &= UpdateVector(F.Arguments, T.Arguments);
            R &= UpdateHeaderSearchDirs(F.HeaderSearchDirs, T.HeaderSearchDirs);
            return R;
          };

#define _(x) PI.x, Out.x
          /*R &=*/ UpdateString(_(Project.Identifier), true);
          /*R &=*/ UpdateString(_(Project.VersionData), true);
          if (!PI.NoCompilationInfo) {
            bool R = true;
            R &= UpdateLanguageBlock(_(Compilation.C));
            R &= UpdateLanguageBlock(_(Compilation.CPP));
            R &= UpdateLanguageBlock(_(Compilation.Default));
            Out.NoCompilationInfo &= R;
          }
#undef _
          if (Out.Directory.empty())
            Out.Directory = PI.Directory;
        });
    }
  };

  mutable std::mutex Mu;
  /// This map is used as persistant storage for cached ProjectInfo files.
  /// Keys are the posix-style ancestor directory. Access to this map is 
  /// guarded by mutex lock.
  mutable llvm::StringMap<ProjectInfoFileCache> Cache;
  const ThreadsafeFS &TFS;
};

} // namespace windroad
} // namespace clangd
} // namespace clang

#endif

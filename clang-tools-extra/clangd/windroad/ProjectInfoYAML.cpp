//===--- PtojectInfoYAML.cpp ----------------------------------------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
#include "ProjectInfo.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/SmallSet.h"
#include "llvm/ADT/SmallString.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/YAMLParser.h"
#include <string>
#include <system_error>

namespace clang {
namespace clangd {
namespace windroad {
namespace {

using llvm::yaml::BlockScalarNode;
using llvm::yaml::MappingNode;
using llvm::yaml::Node;
using llvm::yaml::ScalarNode;
using llvm::yaml::SequenceNode;

class Parser {
  llvm::SourceMgr &SM;
  bool HadError = false;

public:
  Parser(llvm::SourceMgr &SM) : SM(SM) {}

  bool parse(ProjectInfo &P, Node &N) {
    DictParser Dict("ProjectInfo", this);
    Dict.handle("Project", [&](Node &N) { parse(P.Project, N); });
    Dict.handle("Compilation", [&](Node &N) { parse(P, P.Compilation, N); });
    Dict.parse(N);
    return !(N.failed() || HadError);
  }

private:
  void parse(ProjectInfo::ProjectBlock &F, Node &N) {
    DictParser Dict("Project", this);
    Dict.handle("Identifier", [&](Node &N) {
      F.Identifier = scalarValue(N, "Identifier");
    });
    Dict.handle("VersionData", [&](Node &N) {
      F.VersionData = scalarValue(N, "VersionData");
    });
    Dict.parse(N);
  }

  void parse(ProjectInfo &P, ProjectInfo::CompilationBlock &F, Node &N) {
    DictParser Dict("Compilation", this);
    Dict.handle("C", [&](Node &N) { parse(P, F.C, N, "C"); });
    Dict.handle("CPP", [&](Node &N) { parse(P, F.CPP, N, "CPP"); });
    Dict.handle("Default", [&](Node &N) { parse(P, F.Default, N, "Default"); });
    Dict.parse(N);
  }

  void parse(ProjectInfo &P,
             ProjectInfo::CompilationBlock::LanguageBlock &F, Node &N,
             llvm::StringRef LanguageName) {
    DictParser Dict(LanguageName, this);
    Dict.handle("Arguments", [&](Node &N){
      if (auto Values = scalarValues(N)) {
        F.Arguments = std::move(*Values);
        P.NoCompilationInfo = false;
      }
    });
    Dict.handle("HeaderSearchDirs", [&](Node &N){
      if (auto Values = scalarValues(N)) {
        F.HeaderSearchDirs = std::move(*Values);
        P.NoCompilationInfo = false;
      }
    });
    Dict.parse(N);
  }

  // Helper for parsing mapping nodes (dictionaries).
  // We don't use YamlIO as we want to control over unknown keys.
  class DictParser {
    llvm::StringRef Description;
    std::vector<std::pair<llvm::StringRef, std::function<void(Node &)>>> Keys;
    Parser *Outer;

  public:
    DictParser(llvm::StringRef Description, Parser *Outer)
        : Description(Description), Outer(Outer) {}

    // Parse is called when Key is encountered, and passed the associated value.
    // It should emit diagnostics if the value is invalid (e.g. wrong type).
    // If Key is seen twice, Parse runs only once and an error is reported.
    void handle(llvm::StringLiteral Key, std::function<void(Node &)> Parse) {
      for (const auto &Entry : Keys) {
        (void) Entry;
        assert(Entry.first != Key && "duplicate key handler");
      }
      Keys.emplace_back(Key, std::move(Parse));
    }

    // Process a mapping node and call handlers for each key/value pair.
    void parse(Node &N) const {
      if (N.getType() != Node::NK_Mapping) {
        Outer->error(Description + " should be a dictionary", N);
        return;
      }
      llvm::SmallSet<std::string, 8> Seen;
      llvm::SmallVector<std::string, 0> UnknownKeys;
      // We *must* consume all items, even on error, or the parser will assert.
      for (auto &KV : llvm::cast<MappingNode>(N)) {
        auto *K = KV.getKey();
        if (!K) // YAMLParser emitted an error.
          continue;
        auto Key = Outer->scalarValue(*K, "Dictionary key");
        if (!Key)
          continue;
        if (!Seen.insert(*Key).second) {
          Outer->warning("Duplicate key " + *Key + " is ignored", *K);
          if (auto *Value = KV.getValue())
            Value->skip();
          continue;
        }
        auto *Value = KV.getValue();
        if (!Value) // YAMLParser emitted an error.
          continue;
        for (const auto &Handler : Keys) {
          if (Handler.first == *Key) {
            Handler.second(*Value);
            break;
          }
        }
        // Ignore all unknown keys
      }
    }
  };

  // Try to parse a single scalar value from the node, warn on failure.
  llvm::Optional<std::string> scalarValue(Node &N, llvm::StringRef Desc) {
    llvm::SmallString<256> Buf;
    if (auto *S = llvm::dyn_cast<ScalarNode>(&N))
      return std::string(S->getValue(Buf).rtrim().str());
    if (auto *BS = llvm::dyn_cast<BlockScalarNode>(&N))
      return std::string(BS->getValue().rtrim().str());
    warning(Desc + " should be scalar", N);
    return llvm::None;
  }

  // Try to parse a list of single scalar values, or just a single value.
  llvm::Optional<std::vector<std::string>> scalarValues(Node &N) {
    std::vector<std::string> Result;
    if (auto *S = llvm::dyn_cast<ScalarNode>(&N)) {
      llvm::SmallString<256> Buf;
      Result.emplace_back(S->getValue(Buf).str());
    } else if (auto *S = llvm::dyn_cast<BlockScalarNode>(&N)) {
      Result.emplace_back(S->getValue().str());
    } else if (auto *S = llvm::dyn_cast<SequenceNode>(&N)) {
      // We *must* consume all items, even on error, or the parser will assert.
      for (auto &Child : *S) {
        if (auto Value = scalarValue(Child, "List item"))
          Result.push_back(std::move(*Value));
      }
    } else {
      warning("Expected scalar or list of scalars", N);
      return llvm::None;
    }
    return Result;
  }

  // Report a "hard" error, reflecting a config file that can never be valid.
  void error(const llvm::Twine &Msg, llvm::SMRange Range) {
    HadError = true;
    SM.PrintMessage(Range.Start, llvm::SourceMgr::DK_Error, Msg, Range);
  }
  void error(const llvm::Twine &Msg, const Node &N) {
    return error(Msg, N.getSourceRange());
  }

  // Report a "soft" error that could be caused by e.g. version skew.
  void warning(const llvm::Twine &Msg, llvm::SMRange Range) {
    SM.PrintMessage(Range.Start, llvm::SourceMgr::DK_Warning, Msg, Range);
  }
  void warning(const llvm::Twine &Msg, const Node &N) {
    return warning(Msg, N.getSourceRange());
  }
};

} // anonymous namespace

void ProjectInfo::parseYAML(llvm::StringRef YAML,
                            llvm::StringRef BufferName, ProjectInfo &Out) {
  llvm::SourceMgr SM;
  auto Buf = llvm::MemoryBuffer::getMemBufferCopy(YAML, BufferName);

  for (auto &Doc : llvm::yaml::Stream(*Buf, SM)) {
    if (Node *N = Doc.getRoot()) {
      SM.PrintMessage(N->getSourceRange().Start, llvm::SourceMgr::DK_Note,
                       "Parsing project info fragment");
      // Errors and warnings are ignored here
      Parser(SM).parse(Out, *N);
    }
  }
}

} // namespace windroad
} // namespace clangd
} // namespace clang

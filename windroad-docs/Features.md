# New Features in Windroad LLVM

## [clangd] `.ProjectInfo` Database

When there is no compilation database available, clangd will try to
locate `.ProjectInfo` and use it to produce compile arguments. The
`.ProjectInfo` is in YAML format:

```yaml
# Basic project properties
Project:
    # Identifier for this project
    Identifier: 'string'
    # Version information (used by vertool)
    VersionData: 'string'

# Compilation Settings
Compilation:
    # Settings for C Source Files
    C:
        # Arguments to the compiler
        Arguments: [arg1, arg2]
        # Where to find headers
        HeaderSearchDirs: [path1, path2]
    # Settings for C++ Source Files
    CPP:
        # Arguments to the compiler
        Arguments: [arg1, arg2]
        # Where to find headers
        HeaderSearchDirs: [path1, path2]
    # Fallback settings
    Default:
        # Arguments to the compiler
        Arguments: [arg1, arg2]
        # Where to find headers
        HeaderSearchDirs: [path1, path2]
```

## [clangd] Better completion information

Doxygen comments are rendered properly:

![](img/screenshot0.png)

![](img/screenshot1.png)

Completion for macro shows its definition:

![](img/screenshot2.png)

Parameter completion provides formatted document per parameter
and the document of the function:

![](img/screenshot3.png)
